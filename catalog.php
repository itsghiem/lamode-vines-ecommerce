<?php include 'include/header.php';?>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="./index.php">
              <i class="nc-icon nc-bookmark-2"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="active ">
            <a href="./catalog.php">
              <i class="nc-icon nc-basket"></i>
              <p>PRODUCT CATALOG</p>
            </a>
          </li>

          <li>
            <a href="./delivery.php">
              <i class="nc-icon nc-pin-3"></i>
              <p>DELIVERY</p>
            </a>
          </li>

          <li>
            <a href="./user.php">
              <i class="nc-icon nc-single-02"></i>
              <p>ACCOUNTS</p>
            </a>
          </li>
       </ul>
      </div>
    </div>

      <!-- Navbar -->
      <?php include 'include/nav.php';?>
      <div class="container center">
        <h2> PRODUCT CATALOG</h2>
      </div>
      <!-- content -->
    <div class="content ">
      <div class="row">
      <!-- Button trigger modal -->
      <div class="container">
          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add-product">
            ADD PRODUCT
          </button>
          <?php include 'include/../product/add-product.php';?>
          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#add-category">
            ADD CATEGORY
          </button>
          <?php include 'include/../product/add-category.php';?>
      </div>
          <div class="container table-responsive table-scrollable">
            <table id="dtVerticalScrollExample" class="table table-striped table-bordered " cellspacing="0" width="100%">
                <thead class="center">
                  <tr>
                    <th class="th-sm">ID &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">PRODUCT &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">CATEGORY &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">PRICE &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">STOCK &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">ACTION &nbsp
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php 
                          for ($y = 0; $y < 10; $y++)
                          {
                            echo "<tr>
                                    <td>",$y+1,"</td>
                                    <td>AccsadasounDFASDFASDFASDFAS</td>
                                    <td>HOME</td>
                                    <td>123.45</td>
                                    <td>63</td>
                                    <td>
                                    <div class='btn-group ' role='group' aria-label='Basic example'>
                                      <button type='button' class='btn btn-success' data-toggle='modal' data-target='#edit-product'>EDIT</button>
                                        ", include 'include/../product/edit-product.php',"
                                      <button type='button' class='btn btn-danger'>DELETE</button>
                                      ", include 'include/../product/delete-product.php',"
                                  </div>
                                    </td>
                                </tr>";
                          }
                      ?> 
                  </tr>
                </tbody>
              </table>
      </div>
          
      
      <?php include 'include/footer.php';?>