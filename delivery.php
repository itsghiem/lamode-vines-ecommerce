<?php include 'include/header.php';?>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li>
            <a href="./index.php">
              <i class="nc-icon nc-bookmark-2"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li>
            <a href="./catalog.php">
              <i class="nc-icon nc-basket"></i>
              <p>PRODUCT CATALOG</p>
            </a>
          </li>

          <li class="active ">
            <a href="./delivery.php">
              <i class="nc-icon nc-pin-3"></i>
              <p>DELIVERY</p>
            </a>
          </li>

          <li>
            <a href="./user.php">
              <i class="nc-icon nc-single-02"></i>
              <p>ACCOUNTS</p>
            </a>
          </li>
       </ul>
      </div>
    </div>


      <!-- Navbar -->
      <?php include 'include/nav.php';?>
      <div class="container center">
        <h2>DELIVERY DETAILS</h2>
      </div>
      <div class="content">
        <div class="row">
          <div class="container table-responsive table-scrollable">
            <table id="dtVerticalScrollExample" class="table table-striped table-bordered " cellspacing="0" width="100%">
                <thead class="center">
                  <tr>
                    <th class="th-sm">ID
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">USER
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">DESTINATION
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">TYPE
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                    <th class="th-sm">STATUS
                      <i class="fa fa-sort float-right" aria-hidden="true"></i>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php 
                          for ($y = 0; $y < 10; $y++)
                          {
                            echo "<tr>
                                    <td>",$y+1,"</td>
                                    <td>JUAN DELA CRUZ</td>
                                    <td>MAIN BRANCH</td>
                                    <td>PICK-UP</td>
                                    <td>PENDING</td>
                                    </td>
                                </tr>";
                          }
                      ?> 
                  </tr>
                </tbody>
              </table>
          </div>         
        </div>
      </div>
      
      <?php include 'include/footer.php';?>