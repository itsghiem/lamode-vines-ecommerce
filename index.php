<?php include 'include/header.php';?>
      <div class="sidebar-wrapper">
      <ul class="nav">
          <li class="active ">
            <a href="./index.php">
              <i class="nc-icon nc-bookmark-2"></i>
              <p>Dashboard</p>
            </a>
          </li>

          <li>
            <a href="./catalog.php">
              <i class="nc-icon nc-basket"></i>
              <p>PRODUCT CATALOG</p>
            </a>
          </li>

          <li>
            <a href="./delivery.php">
              <i class="nc-icon nc-pin-3"></i>
              <p>DELIVERY</p>
            </a>
          </li>

          <li>
            <a href="./user.php">
              <i class="nc-icon nc-single-02"></i>
              <p>ACCOUNTS</p>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
    
      <!-- Navbar -->
      <?php include 'include/nav.php';?>
      <div class="container center">
        <h2>DASHBOARD</h2>
      </div>
      <!-- Content -->
      <div class="content">
        <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-single-02 text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">USER ACCOUNT</p>
                      <p class="card-title"> <!-- INSERT sql query --> 2<p> 
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-money-coins text-success"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Daily Total Revenue</p>
                      <p class="card-title"> <!-- INSERT sql query --> 1,345
                        <p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-vector text-danger"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Pending Delivery</p>
                      <p class="card-title"> <!-- INSERT sql query -->23
                        <p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-favourite-28 text-primary"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <p class="card-category">Followers</p>
                      <p class="card-title">+45K
                        <p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
      </div>



      <?php include 'include/footer.php';?>