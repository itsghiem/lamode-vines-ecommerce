<!-- Modal -->
<div class="modal fade" id="edit-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EDIT PRODUCT</h5>
      </div>
      <div class="modal-body">
        
        <form>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>PRODUCT</label>
                        <input type="text" class="form-control" placeholder="product" value="PRODUCT NAME" disabled>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>CATEGORY</label>
                        <br>
                        <select  class="form-control" name="category" form="category-form">
                        <?php 
                          for ($x = 0; $x <= 10; $x++)
                          {    
                            echo"<option value='",$x+1,"'>",$x,"</option>";
                          }?>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>PRICE</label>
                        <input type="text" class="form-control" placeholder="price" value="0.00">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>STOCK</label>
                        <input type="number" class="form-control" placeholder="1" min="1" max="100">
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>PRODUCT Description</label>
                        <input type="textarea" class="form-control" placeholder="product" required>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                <label>UPLOAD PHOTO</label>
                    <div class="file">
                        <input class="file" type="file" name="file"/>
                    </div>
                </div>
            </div>
            
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" data-dismiss="modal"> Save changes</button>
      </div>
    </div>
  </div>
</div>