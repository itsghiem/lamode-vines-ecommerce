
<!-- Modal -->
<div class="modal fade" id="add-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ADD CATEGORY</h5>
      </div>
      <div class="modal-body">
        <form>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>CATEGORY NAME</label>
                        <input type="text" class="form-control" placeholder="category" value="CATEGORY NAME">
                    </div>
                </div>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>
</div>